require 'json'

class Plop
  @@id = 0

  attr_accessor :parent, :rank

  def initialize(children = [], **hash)
    id = (@@id += 1)
    @children = children
    @children.each_with_index do |child, idx|
      child.update({
        "parentId" => id,
        "rank" => idx,
      })
    end

    @hash = {
      "id" => id,
      "parentId" => nil,
      "data" => nil,
      "rank" => 1,
      "character_id" => 1,
    }.update(hash)
  end

  def update(hash)
    @hash.update hash
  end

  class << self
    def tab(rank, name, children)
      new(children, type: "tab-section", value: name, rank: rank)
    end

    def number(name, value, formula: nil, message: nil)
      new(type: "number", name: name, value: value, formula: formula, message: message)
    end

    def title_section(name, children)
      new(children, type: "title-section", data: {collapsed: false}, value: name)
    end

    def paragraph(value)
      new(type: "paragraph", value: value)
    end

    def message(name, message)
      new(type: "message", name: name, message: message)
    end

    def text(name, value, formula: nil, message: nil)
      new(type: "text", name: name, value: value, formula: formula, message: message)
    end

    def horizontal_section(columns)
      new(columns.map{ section(100.0 / columns.count, _1) }, type: "horizontal-section")
    end

    def section(size, children)
      new(children, type: "section")
    end

    def hit_points(name, maximum)
      children = [
        number("#{name}-maximum", maximum),
        number("#{name}-temporary", 0),
      ]
      new(children, name: name, type: "health")
    end

    def checkboxes(name, maximum, value=0, formula: nil, message: nil)
      child = number("#{name}-max", maximum)
      new([child], type: "checkboxes", name: name, value: value, formula: formula, message: message)
    end

    def heading(value)
      new(type: "heading", value: value)
    end

    def appearance
      new(type: "appearance", data: {appearances: []})
    end

    def local(thing)
      thing.update({local: true})
      thing
    end

    def sheet(hash)
      dcs = hash[:dcs].map do |item|
        number(item[0], item[1])
      end
      
      crunch = tab 1, "Crunch", [
        heading("#{hash[:name]} - #{hash[:kind]} (#{hash[:traits].join(", ")}) #{hash[:level]}"),
        horizontal_section([
          [
            local(hit_points("Hit-Points", hash[:hp])),
          ],
          [
            (paragraph("<p><b>Immunities:</b> #{hash[:immunities]}</p>") if hash[:immunities]),
            (paragraph("<p><b>Weaknesses:</b> #{hash[:weaknesses]}</p>") if hash[:weaknesses]),
          ].compact,
        ]),
        horizontal_section([
          [
            local(text("Speed", hash[:speed])),
            local(number("Armor Class", hash[:ac])),   
            local(text("Perception", hash[:perception])), # TODO break out bonus into messageable number field
          ],
          [
            local(number("Fortitude", hash[:fort], message: "Fortitude: {d20+Fortitude}")),
            local(number("Reflex", hash[:ref], message: "Reflex: {d20+Reflex}")),
            local(number("Will", hash[:will], message: "Will: {d20+Will}")),
          ],
        ]),
        horizontal_section([
          [
            local(number("Str", hash[:str], message: "Str: {d20+Str}")),
            local(number("Dex", hash[:dex], message: "Dex: {d20+Dex}")),
            local(number("Con", hash[:con], message: "Con: {d20+Con}")),
          ],
          [
            local(number("Int", hash[:int], message: "Int: {d20+Int}")),
            local(number("Wis", hash[:wis], message: "Wis: {d20+Wis}")),
            local(number("Cha", hash[:cha], message: "Cha: {d20+Cha}")),
          ],
        ]),
        title_section("Skills", [
          *hash[:skills].map do |skill|
            [
              local(number(skill[:name], skill[:bonus], message: "#{skill[:name]}: {d20+#{skill[:name]}}")),
              *(paragraph(skill[:description]) unless skill[:description].to_s.empty?),
            ]
          end.flatten,
        ]),
        heading("Attacks"),
        checkboxes("Attack Counter", 2, message: "Attack Counter reset to {AttackCounter = 0} !h"),
        *hash[:activities].map do |activity|
          title_section(activity[:name], [
            paragraph("<p>#{['', '1 action: '][activity[:actions]] || "#{[activity[:actions]]} actions: "}#{activity[:description]}</p>"),
            horizontal_section([
              if activity[:attack].nil?
                []
              else
                [
                  number(activity[:name], activity[:attack],
                    formula: "#{activity[:attack]} - (min(2, AttackCounter) * #{activity[:attack_penalty]})",
                    message: "#{activity[:name]}: {d20 + #{activity[:name]}} (attack {AttackCounter = AttackCounter + 1})",
                  )
                ]
              end,
              activity[:effects].map{ |effect| local text "#{activity[:name]} Effect", effect, message: "#{activity[:name]} effect: {#{effect}}" },
            ]),
          ])
        end,
      ]
      fluff = tab 2, "Fluff", [
        title_section("Recall Knowledge DCs", dcs),
        paragraph(hash[:description]),
        *hash[:lore].map do |lore|
          title_section(lore[:title], [
            paragraph(lore[:text])
          ])
        end,
        paragraph("<p><b>Source:</b> #{hash[:source]}</p>"),
        appearance(),
      ]

      {
        "properties" => [*crunch.flatten, *fluff.flatten],
        "private" => false,
        "type" => "tableplop-character-v2",
      }
    end
  end

  def each_hash
    yield @hash
    @children.each{ |child| child.each_hash{ yield _1 } }
  end

  def flatten
    ret = []
    each_hash{ ret.push _1 }
    ret
  end
end
