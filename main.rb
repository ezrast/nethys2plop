require 'nokogiri'
require 'open-uri'
require 'json'
require 'pp'

require_relative 'plop'

unless ARGV.count == 1
  STDERR.puts "Usage: ruby main.rb <Path or URL>"
end

def scrape_stat_blocks(url)
  doc = Nokogiri::HTML(URI.open(url))

  stat_blocks = doc.xpath '//span[@id="ctl00_RadDrawer1_Content_MainContent_DetailedOutput"]'
  stat_blocks_data = []

  stat_blocks.each do |block|
    data = {
      dcs: [],
      unknown_elements: [],
      activities: [],
      traits: [],
      lore: [],
    }

    def data.set_warn(key, value)
      unless self[key].nil?
        STDERR.puts "Overwriting #{key}: was #{self[key]}, now #{value}"
      end
      self[key] = value
    end

    children = block.children

    if children.first.node_name == "h1" && children.first["class"] == "title"
      data[:name] = children.shift.content
    end

    description = ""
    while true
      if children.first.text?
        description << "<p>#{children.shift.content}</p>"
      elsif children.first.node_name == "br"
        children.shift
      else
        break
      end
    end
    data.set_warn(:description, description)

    until children.empty?
      if children.first.node_name == "br" || children.first.node_name == "hr"
        children.shift
      # Difficulty modifier links
      elsif children.first.node_name == "h2" && children.first.children.first.node_name == "a" && children.first.children.first.content == "Elite"
        children.shift
      # Knowledge DCs
      elsif children[0].node_name == "b" && children[1].content.match(/: DC (\d+)/)
        data[:dcs].push [
          children[0].content.split('Recall Knowledge - ').join,
          Integer($1)
        ]
        children.shift
        children.shift
      # Name and level
      elsif children.first.node_name == "h1" && children.first.children.last.node_name == "span" && children.first.children.last.content.match(/(.+) (\d+)/)
        data.set_warn(:kind, $1)
        data.set_warn(:level, Integer($2))
        children.shift
      # Thumbnail
      elsif children.first.node_name == "a" && children.first.children.first["class"] == "thumbnail"
        children.shift
      # Rarity
      elsif children.first["class"] == "traitrare"
        data.set_warn(:rarity, children.shift.content)
      # Alignment
      elsif children.first["class"] == "traitalignment"
        data.set_warn(:alignment, children.shift.content)
      # Spacing
      elsif children.first["style"] == "letter-spacing: -.4em"
        children.shift
      # Size
      elsif children.first["class"] == "traitsize"
        data.set_warn(:size, children.shift.content)
      # Trait
      elsif children.first["class"] == "trait"
        data[:traits].push(children.shift.content)
      # Source
      elsif children.first.content == "Source"
        children.shift
        source = ''
        until (child = children.shift).node_name == "br"
          source << child.content
        end
        data.set_warn(:source, source.strip)
      # Skills
      elsif children.first.content == "Skills"
        children.shift
        skill_text = ''
        until (child = children.shift).node_name == "br"
          skill_text << child.content
        end
        parsed_length = 0
        skill = skill_text.split(", ")
        skills = skill_text.scan(/([^+,]+ )([-+]\d+)([^,]*)(, |$)/).map() do |match|
          parsed_length += match.map(&:length).sum
          {
            name: match[0].strip,
            bonus: Integer(match[1]),
            description: match[2].strip,
          }
        end
        if parsed_length != skill_text.length
          data[:unknown_elements].push skill_text
        end
        data.set_warn(:skills, skills)
      # Perception
      elsif children.first.content == "Perception"
        children.shift
        perception_text = ''
        until (child = children.shift).node_name == "br"
          perception_text << child.content
        end
        data.set_warn(:perception, perception_text.strip)
      # Speed
      elsif children.first.content == "Speed"
        children.shift
        speed_text = ''
        until (child = children.shift).node_name == "br"
          speed_text << child.content
        end
        data.set_warn(:speed, speed_text)
      # Abilities
      elsif children.first["class"] == "hanging-indent"
        children.shift.tap do |item|
          inner_children = item.children
          name = inner_children.shift.content
          actions = 0
          value = ''
          inner_children.each do |child|
            if child["title"] == "Single Action"
              actions = 1
            else
              value << child.content
            end
          end
          if name == "Melee" && value.match(/([^+]+) \+/)
            name = $~[1].strip.split.map{ _1[0].upcase + _1[1..] }.join
          end
          attack = nil
          attack_penalty = nil
          if value.match(/([-+]\d+) \[([-+]\d+)\/([-+]\d+)\]/)
            attack = Integer($~[1])
            attack_penalty = Integer($~[1]) - Integer($~[2])
          end
          effects = []
          value.scan(/\d+d\d+(?:[-+]\d+)?/) do |match|
            effects.push(match.to_s)
          end

          data[:activities].push({
            name: name,
            description: value.strip,
            actions: actions,
            attack: attack,
            attack_penalty: attack_penalty,
            effects: effects,
          })
        end
      elsif %w[Str Dex Con Int Wis Cha AC Fort Ref Will HP].include? children.first.content
        data.set_warn(children.shift.content.downcase.to_sym, Integer(children.shift.content.strip.chomp(',').chomp(';')))
      elsif %w[Immunities Weaknesses].include? children.first.content
        kind = children.shift.content.downcase.to_sym
        content = ''
        until children.first.node_name == "b" || children.first.node_name == "br"
          content << children.shift.content
        end
        data.set_warn(kind, content.strip.chomp(',').chomp(';'))
      # More abilities
      elsif children[0].node_name == "b" && children[1].text?
        data[:activities].push({
          name: children.shift.content,
          description: children.shift.content,
          actions: 0,
          attack: nil,
          attack_penalty: nil,
          effects: [],
        })
      elsif children[0].node_name == "h3" && children[1].text?
        data[:lore].push({
          title: children.shift.content,
          text: children.shift.content,
        })
      elsif children.first.node_name == "h2" && children.first.content.start_with?("All Monsters in ")
        break
      else
        data[:unknown_elements].push(children.shift)
      end
    end

    data[:rarity] ||= "Common"
    
    stat_blocks_data << data
  end
  

  stat_blocks_data
end

# Call the scraping function
hash = scrape_stat_blocks(ARGV[0])
PP.pp hash, STDERR
puts Plop.sheet(hash.first).to_json